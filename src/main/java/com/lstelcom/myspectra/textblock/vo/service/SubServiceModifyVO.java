package com.lstelcom.myspectra.textblock.vo.service;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.lstelcom.myspectra.base.common.BaseVO;
import io.micronaut.core.annotation.Introspected;
import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

/**
 * @author:wy
 * @date:2020-8-6
 */
@Introspected
@Schema(
        name = "SubServiceModifyVO"
)
public class SubServiceModifyVO extends BaseVO {

    @Schema(name = "id", description = "ID")
    private Integer id;

    @Schema(name = "code", description = "Subservice code: LOV(804, etc.) ")
    @NotNull
    @NotBlank
    private String code;

    @Schema(name = "status", description = "Status of service: LOV{active;inactive}")
    @NotNull
    @NotBlank
    private String status;

    @Schema(name = "periodCategory", description = "Licence period category: LOV{lifetime, renewable, " +
            "non-renewable, temporary} ")
    @NotNull
    @NotBlank
    private String periodCategory;

    @Schema(name = "periodUnit", description = "Licence period unit: LOV{week, month, year}")
    @NotNull
    @NotBlank
    private String periodUnit;

    @Schema(name = "period", description = "Licence period: integer ")
    @NotNull
    private Integer period;

    @Schema(name = "startDate", description = "Availability Start date")
    @NotNull
    @NotBlank
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate startDate;

    @Schema(name = "endDate", description = "Availability End date")
    @NotNull
    @NotBlank
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate endDate;

    @Schema(name = "techAnalysis", description = "Technical Analysis: LOV{required, not required} ")
    @NotNull
    @NotBlank
    private String techAnalysis;

    @Schema(name = "automation", description = "Automatic flow in workflow: LOV{auto, manual} ")
    @NotNull
    @NotBlank
    private String automation;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPeriodCategory() {
        return periodCategory;
    }

    public void setPeriodCategory(String periodCategory) {
        this.periodCategory = periodCategory;
    }

    public String getPeriodUnit() {
        return periodUnit;
    }

    public void setPeriodUnit(String periodUnit) {
        this.periodUnit = periodUnit;
    }

    public Integer getPeriod() {
        return period;
    }

    public void setPeriod(Integer period) {
        this.period = period;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public String getTechAnalysis() {
        return techAnalysis;
    }

    public void setTechAnalysis(String techAnalysis) {
        this.techAnalysis = techAnalysis;
    }

    public String getAutomation() {
        return automation;
    }

    public void setAutomation(String automation) {
        this.automation = automation;
    }

    @Override
    public String toString() {
        return "SubServiceNewVO{" +
                "code='" + code + '\'' +
                ", status='" + status + '\'' +
                ", periodCategory='" + periodCategory + '\'' +
                ", periodUnit='" + periodUnit + '\'' +
                ", period=" + period +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", techAnalysis='" + techAnalysis + '\'' +
                ", automation='" + automation + '\'' +
                '}';
    }
}
