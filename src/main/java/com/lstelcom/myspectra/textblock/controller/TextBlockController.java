package com.lstelcom.myspectra.textblock.controller;

import com.lstelcom.myspectra.base.common.ContextDTO;
import com.lstelcom.myspectra.base.common.Result;
import com.lstelcom.myspectra.base.common.ResultCode;
import com.lstelcom.myspectra.textblock.service.TextBlockService;
import com.lstelcom.myspectra.textblock.vo.service.SubServiceModifyVO;
import com.lstelcom.myspectra.textblock.vo.service.SubServiceNewVO;

import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.*;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.jooq.DSLContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.sql.DataSource;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;


/**
 * @author wy130132295
 */
@Controller("/api")
//@Tag(name = "TextController")
public class TextBlockController {

    private static final Logger logger = LoggerFactory.getLogger(TextBlockController.class);


    @Get(value = "/subService", produces = MediaType.APPLICATION_JSON)
    /*@Operation(
            summary = "Get sub-service",
            description = "To get a sub-service by id"
    )*/
    public String getSubService(
            
    ) {
		return "textblock";
      
    	
    }

    @Get("/") 
    public String index() {
        return "Hello Micronaut";
    }

   


}
