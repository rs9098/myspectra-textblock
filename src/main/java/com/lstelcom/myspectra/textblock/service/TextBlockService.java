package com.lstelcom.myspectra.textblock.service;

import com.lstelcom.myspectra.base.common.ContextDTO;
import com.lstelcom.myspectra.textblock.vo.service.SubServiceModifyVO;
import com.lstelcom.myspectra.textblock.vo.service.SubServiceNewVO;

public interface TextBlockService {

    /**
     * insertSubService
     * @param vo
     * @return
     */
    ContextDTO insertSubService(SubServiceNewVO vo);

    /**
     * getSubService
     * @param id
     * @return
     */
    ContextDTO<SubServiceModifyVO> getSubService(Integer id);

    /**
     * listSubService
     * @return
     */
    ContextDTO<SubServiceModifyVO> listSubService();

    /**
     * deleteSubService
     * @param id
     * @return
     */
    ContextDTO deleteSubService(Integer id);

    /**
     * updateSubService
     * @param vo
     * @return
     */
    ContextDTO updateSubService(SubServiceModifyVO vo);
}
