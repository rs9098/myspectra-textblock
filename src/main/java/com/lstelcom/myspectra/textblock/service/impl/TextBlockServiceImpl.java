package com.lstelcom.myspectra.textblock.service.impl;

import com.lstelcom.myspectra.base.common.ContextDTO;
import com.lstelcom.myspectra.textblock.service.TextBlockService;
import com.lstelcom.myspectra.textblock.vo.service.SubServiceModifyVO;
import com.lstelcom.myspectra.textblock.vo.service.SubServiceNewVO;

//import com.lstelcom.spweb.wsclient.lov.MySpLovClient;
import org.jooq.DSLContext;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author:wy
 * @date:2020-7-29
 */
@Singleton
public class TextBlockServiceImpl implements TextBlockService {

    @Inject
    DSLContext dsl;

  //  ProdSubServiceDao prodSubServiceDao;

    @Inject
    public void setProdSubServiceDao() {
   //     prodSubServiceDao = new ProdSubServiceDao(dsl.configuration());
    }

    @Override
    public ContextDTO insertSubService(SubServiceNewVO vo) {
        ContextDTO rs = new ContextDTO();
     //   MySpLovClient lovClient = MySpLovClient.getInstance();
       /* lovClient.convertKey("COMMON", "C_STATUS", vo.getStatus());
        lovClient.convertKey("COMMON", "C_PERIOD_CATEGORY", vo.getPeriodCategory());
        lovClient.convertKey("COMMON", "C_PERIOD_UNIT", vo.getPeriodUnit());
        lovClient.convertKey("COMMON", "C_TECH_ANALYSIS", vo.getTechAnalysis());
        lovClient.convertKey("COMMON", "C_PROD_AUTOMATION", vo.getAutomation());
        ProdSubService prodSubService = vo.convertTo(ProdSubService.class);
        prodSubService.setId(dsl.nextval(Sequences.PROD_SUB_SERVICE_PK_SEQ).intValue());
        prodSubServiceDao.insert(prodSubService);*/
        return rs;
    }

    @Override
    public ContextDTO<SubServiceModifyVO> getSubService(Integer id) {
        ContextDTO<SubServiceModifyVO> rs = new ContextDTO<>();
      //  ProdSubService prodSubService = prodSubServiceDao.fetchOneById(id);
        //if (prodSubService != null) {
        //    rs.setData((SubServiceModifyVO) new SubServiceModifyVO().convertFrom(prodSubService));
        //}
        return rs;
    }

    @Override
    public ContextDTO<SubServiceModifyVO> listSubService() {
        ContextDTO<SubServiceModifyVO> rs = new ContextDTO<>();
        //List<ProdSubService> all = prodSubServiceDao.findAll();
        //return rs.setRows(all.stream().map(l -> (SubServiceModifyVO) new SubServiceModifyVO().convertFrom(l)).collect(Collectors.toList()));
   return rs;
    }

    @Override
    public ContextDTO deleteSubService(Integer id) {
       // prodSubServiceDao.deleteById(id);
        return new ContextDTO();
    }

    @Override
    public ContextDTO updateSubService(SubServiceModifyVO vo) {
        ContextDTO rs = new ContextDTO();
       // MySpLovClient lovClient = MySpLovClient.getInstance();
       /* lovClient.convertKey("COMMON", "C_STATUS", vo.getStatus());
        lovClient.convertKey("COMMON", "C_PERIOD_CATEGORY", vo.getPeriodCategory());
        lovClient.convertKey("COMMON", "C_PERIOD_UNIT", vo.getPeriodUnit());
        lovClient.convertKey("COMMON", "C_TECH_ANALYSIS", vo.getTechAnalysis());
        lovClient.convertKey("COMMON", "C_PROD_AUTOMATION", vo.getAutomation());
        ProdSubService prodSubService = vo.convertTo(ProdSubService.class);
        prodSubServiceDao.update(prodSubService);
       */ return rs;
    }
}
