package com.lstelcom.myspectra;

import io.micronaut.runtime.Micronaut;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.info.License;

@OpenAPIDefinition(
        info = @Info(
                title = "myspectra-textblock",
                version = "${api.version}",
                description = "${openapi.description}",
                license = @License(name = "Apache 2.0", url = "https://foo.bar"),
                contact = @Contact(
                        url = "https://gigantic-server.com",
                        name = "CaoShun",
                        email = "cao.shun@ng-networks.cn")
        )
)
public class Application {

    public static void main(String[] args) {
    	System.out.println("Before start**********");
        Micronaut.run(Application.class);
        System.out.println("After start*********");
    }

}
