package com.lstelcom.myspectra.base.common;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * @author:wy
 * @date:2020/2/18
 */
@Setter
@Data
@NoArgsConstructor
@Accessors(chain = true)
public class ContextDTO<T> {

    private T data;

    private boolean success = true;

    private String message;

    private List<T> rows;

    public ContextDTO<T> convertMessage(ContextDTO dto){
        return this.setSuccess(dto.isSuccess()).setMessage(dto.getMessage());
    }

}
