package com.lstelcom.myspectra.base.common;


import com.lstelcom.myspectra.base.execption.CommonException;
import net.sf.cglib.beans.BeanCopier;

/**
 * @author:wy
 * @date:2020/1/17
 */
public abstract class BaseVO {

    public <T> T convertTo(Class<T> clazz) {
        try {
            Object o = clazz.newInstance();
            BeanCopier copier = BeanCopier.create(this.getClass(), clazz, false);
            copier.copy(this, o, null);
            return (T) o;
        } catch (Exception e) {
            e.printStackTrace();
            throw new CommonException(clazz.getName() + " convert failed : " + this);
        }
    }

    public BaseVO convertFrom(Object o) {
        try {
            BeanCopier copier = BeanCopier.create(o.getClass(), this.getClass(), false);
            copier.copy(o, this, null);
            return this;
        } catch (Exception e) {
            e.printStackTrace();
            throw new CommonException(o + " convert failed");
        }
    }

}
