package com.lstelcom.myspectra.base.common;

import io.swagger.v3.oas.annotations.media.Schema;

import java.util.List;

/**
 * @author wy130132295
 */
@Schema(name = "Result")
public class Result<T> {

    /**
     * result data
     */
    @Schema(name = "data", description = "data")
    private T data;

    /**
     * result status
     */
    @Schema(name = "code", description = "code")
    private Integer code;

    /**
     * result message
     */
    @Schema(name = "message", description = "message")
    private String message;

    /**
     * row data total number
     */
    @Schema(name = "total", description = "total")
    private Integer total;

    /**
     * rows for table data
     */
    @Schema(name = "rows", description = "rows")
    private List<T> rows;


    public Result() {
        this.setCode(ResultCode.SUCCESS);
    }

    public Result(ResultCode code) {
        this.setCode(code);
    }


    public Result<T> setCode(ResultCode code) {
        this.code = code.getCode();
        this.message = code.getMessage();
        return this;
    }

    public T getData() {
        return data;
    }

    public Result<T> setData(T data) {
        this.data = data;
        return this;
    }

    public Integer getCode() {
        return code;
    }

    public Result<T> setCode(Integer code) {
        this.code = code;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public Result<T> setMessage(String message) {
        this.message = message;
        return this;
    }

    public Integer getTotal() {
        return total;
    }

    public Result<T> setTotal(Integer total) {
        this.total = total;
        return this;
    }

    public List<T> getRows() {
        return rows;
    }

    public Result<T> setRows(List<T> rows) {
        this.rows = rows;
        return this;
    }
}
