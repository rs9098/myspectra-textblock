package com.lstelcom.myspectra.base.common;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author wy130132295
 */

@SuppressWarnings("DELETE")
@AllArgsConstructor
@Getter
public enum ResultCode {

    /**
     * all of the errors begin with '9'
     * 9999:Unknown error
     * 9000:Application error
     * 9001:Validate error
     * 9002:Logical error
     * 9003:Permission error
     * -1:other error
     */
    SUCCESS(0, "Success"),
    ERROR(9999, "Unknown error"),
    APPLICATION_ERROR(9000, "Application error"),
    VALIDATE_ERROR(9001, "Validate error"),
    LOGICAL_ERROR(9002, "Logical error"),
    PERMISSION_ERROR(9003, "Permission error");

    private int code;
    private String message;

}
