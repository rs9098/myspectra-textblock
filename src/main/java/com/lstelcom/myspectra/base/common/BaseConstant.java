package com.lstelcom.myspectra.base.common;

/**
 * @author:wy
 * @date:2020/1/9
 */
public interface BaseConstant {


    interface NumberStr {

        String ZERO = "0";
        String ONE = "1";
        String TWO = "2";
        String THREE = "3";

    }

    interface Flag {

        Integer TRUE = 1;

        Integer FALSE = 0;
    }

    interface Number {

        int ZERO = 0;
        int ONE = 1;
        int TWO = 2;
        int THREE = 3;

    }


    interface CommonError {

        String DELETE_ERROR = "Delete record failed : Not found";

        String UPDATE_ERROR = "Update record failed : Illegal parameter";

        String INSERT_ERROR = "Insert record failed : Illegal parameter";

        String NOT_FOUND = "Record not found";

        String DB_ERROR = "Database operation error";

    }

    interface ValidError {

        String NOTNULL = " : can't be null";

        String NOTBLANK_FIELD = " : can't be blank";

        String NOTBLANK_PARAM = " can't be blank";

        String LENGTH_LIMIT = " : length can't exceed ";

        String LENGTH_START = " : length must be larger than ";

        String NUMBER_LIMIT_END = " : number can't be larger than ";

        String NUMBER_LIMIT_START = " : number can't be less than ";

        String MUST_BE_NUMBER = " : must be a number";


    }

    interface Character {

        String NEWLINE = "\n";

        String SPACE = " ";

        String SEMICOLON = ";";

        String SLASH = "/";

    }


}
