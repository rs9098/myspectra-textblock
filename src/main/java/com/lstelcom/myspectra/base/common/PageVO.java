package com.lstelcom.myspectra.base.common;

import com.lstelcom.myspectra.base.execption.CommonException;
import com.lstelcom.myspectra.base.utils.DateUtils;
import com.lstelcom.myspectra.base.utils.JsonUtils;
import com.lstelcom.myspectra.base.vo.common.FilterItemVo;
import com.lstelcom.myspectra.base.vo.common.SortItemVo;
import com.lstelcom.myspectra.base.vo.common.WhereFieldVo;
//import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.jooq.Result;
import org.jooq.*;
import org.jooq.impl.DSL;
import org.jooq.impl.DefaultDSLContext;
import org.jooq.tools.StringUtils;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.*;
import java.util.stream.Collectors;

//import static com.lstelcom.myspectra.base.common.PageVO.JooqUtil.Operator.*;
//import static com.lstelcom.myspectra.base.common.PageVO.JooqUtil.Operator.*;
//import static com.lstelcom.myspectra.base.common.PageVO.JooqUtil.Order.*;


/**
 * @author wy130132295
 */
/*@Getter
@Setter
@Data
@ApiModel("PageVO")
public class PageVO<T> {

    private String param1;

    private String param2;

    private String param3;

    private String param4;

    private String param5;

    private String tableName;

    *//**
     * [
     * String CONTAIN = "contain";
     * String GREAT = "great";
     * String LESS = "less";
     * String EQUAL = "equal";
     * String IN = "in";
     * {
     * field: 'feeStatus',
     * operator: 'equal',/great less contain
     * value: this.searchParam.payStatus
     * }
     *//*
    private String filterRules;

    *//**
     * [
     * {
     * field : "xxx",
     * order : 'desc'/'asc'
     * }
     * ]
     *//*
    private String sortRules;

    private List<T> rows;

    private List<Map<String, Object>> maps;

    private Integer total;

    private Integer page;

    private Integer pageSize;

    private Boolean auth;


    public PageVO() {
    }

    public PageVO(int page, int pageSize) {
        this.page = page;
        this.pageSize = pageSize;
    }

    private SelectWithTiesAfterOffsetStep<?> applyParam(SelectConditionStep<?> where) throws IOException {
        //ç­›é€‰
        JooqUtil.applyFilter(where, filterRules);
        JooqUtil.applyGroupBy(where);
        //è®¡æ•°
        total = JooqUtil.count(where);
        //æŽ’åº�
        SelectSeekStepN<?> sortSql = JooqUtil.applySort(where, sortRules);
        //åˆ†é¡µ
        init();
        Integer start = (page - 1) * pageSize;
        Integer pageSize = this.pageSize;
        SelectWithTiesAfterOffsetStep<?> limit;
        if (sortSql != null) {
            limit = sortSql.limit(start, pageSize);
        } else {
            limit = where.limit(start, pageSize);
        }
        return limit;
    }

    public void applyAndFetch(SelectConditionStep<?> where, Class<T> clazz) throws IOException {
        SelectWithTiesAfterOffsetStep<?> limit = applyParam(where);
        this.rows = limit.fetchInto(clazz);
        recheck();
    }

    public void applyAndFetchMap(SelectConditionStep<?> where) throws IOException {
        SelectWithTiesAfterOffsetStep<?> limit = applyParam(where);
        Result<?> fetch = limit.fetch();
        if (fetch != null) {
            List<Map<String, Object>> maps = fetch.intoMaps();
            this.maps = maps.stream()
                    .map(m ->
                            {
                                Map<String, Object> map = new HashMap<>(m.size());
                                m.forEach((key, value) -> map.put(JooqUtil.tranFieldDBToPojo(key), value));
                                return map;
                            }
                    )
                    .collect(Collectors.toList());
        } else {
            this.maps = new ArrayList<>();
        }
    }

    private void init() {
        if (this.page == null) {
            this.page = 1;
        }
        if (this.pageSize == null) {
            this.pageSize = 10;
        }
    }

    private void recheck() {
        if (this.rows == null) {
            this.rows = new ArrayList<>();
        }
    }


    public static class JooqUtil {

        private static ThreadLocal<FilterItemVo> preFilterThreadLocal = new ThreadLocal<>();

        private static ThreadLocal<List<Field>> preGroupByThreadlocal = new ThreadLocal<>();

        private static ThreadLocal<List<FilterItemVo>> preFilterListThreadLocal = new ThreadLocal<>();

        *//**
         * è®¾ç½®é¢„ç½®çš„ç­›é€‰æ�¡ä»¶
         *
         * @param preFilterVo
         *//*
        public static void setPreFilter(FilterItemVo preFilterVo) {
            preFilterThreadLocal.set(preFilterVo);
        }

        public static void addPreFilter(List<FilterItemVo> preFilterVOs) {
            preFilterListThreadLocal.set(preFilterVOs);
        }

        public static void addGroupBy(List<Field> fields) {
            preGroupByThreadlocal.set(fields);
        }

        public static void applyGroupBy(SelectConditionStep<?> where) {
            List<Field> fields = preGroupByThreadlocal.get();
            if (fields != null && fields.size() > 0) {
                where.groupBy(fields);
            }
            preGroupByThreadlocal.remove();
        }


        public interface Operator {

            String CONTAIN = "contain";

            String GREAT = "great";

            String LESS = "less";

            String EQUAL = "equal";

            String GREATOREQUAL = "greatorequal";

            String LESSOREQUAL = "lessorequal";

            String IN = "in";

            List<String> ALL = Arrays.asList(CONTAIN, GREAT, LESS, EQUAL, IN, GREATOREQUAL, LESSOREQUAL);

        }


        interface Order {

            String DESC = "desc";

            String ASC = "asc";

        }

        public static void applyFilterList(SelectConditionStep<?> where, String filterJson) throws IOException {
            List<FilterItemVo> filterItemVos = preFilterListThreadLocal.get();
            if (!StringUtils.isBlank(filterJson) || filterItemVos != null) {
                List<FilterItemVo> filterItemVoList = null;
                if (!StringUtils.isBlank(filterJson)) {
                    filterItemVoList = tranFilterRulesJson(filterJson);
                }
                if (filterItemVos != null) {
                    if (filterItemVoList == null) {
                        filterItemVoList = new ArrayList<>(16);
                    }
                    filterItemVoList.addAll(filterItemVos);
                }
                applyFilterItem(where, filterItemVoList);
            }

            preFilterListThreadLocal.remove();
        }

        *//**
         * ç­›é€‰æ�¡ä»¶apply
         *
         * @param where
         * @param filterJson
         * @throws IOException
         *//*
        public static void applyFilter(SelectConditionStep<?> where, String filterJson) throws IOException {
            FilterItemVo preItem = preFilterThreadLocal.get();
            if (!StringUtils.isBlank(filterJson) || preItem != null) {
                List<FilterItemVo> filterItemVos = null;
                if (!StringUtils.isBlank(filterJson)) {
                    filterItemVos = tranFilterRulesJson(filterJson);
                }
                if (preItem != null) {
                    if (filterItemVos == null) {
                        filterItemVos = new ArrayList<>(16);
                    }
                    filterItemVos.add(preFilterThreadLocal.get());
                    preFilterThreadLocal.remove();
                }
                applyFilterItem(where, filterItemVos);
            }
        }

        private static void applyFilterItem(SelectConditionStep<?> where, List<FilterItemVo> filterItemVos) {
            if (filterItemVos != null) {
                Map<String, WhereFieldVo> whereFieldMap = getWhereFiled(where);
                filterItemVos.forEach((FilterItemVo x) -> {
                    if (whereFieldMap.containsKey(x.getField())) {
                        String operator = x.getOperator();
                        if (StringUtils.isBlank(operator)) {
                            return;
                        }
                        WhereFieldVo vo = whereFieldMap.get(x.getField());
                        Field field = vo.getField();
                        //å°†aliasåˆ‡æ�¢è‡³è¡¨åŽŸç”Ÿå­—æ®µ
                        field = wrap(field);
                        String value = x.getValue();
                        Object typeValue = null;
                        try {
                            typeValue = tranObjectType(value, field.getDataType().getType(), operator);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        if (typeValue == null) {
                            throw new CommonException("æš‚ä¸�æ”¯æŒ�çš„æ•°æ�®ç±»åž‹:" + field.getDataType().getType());
                        }
                        switch (operator) {
                            case CONTAIN:
                                if (typeValue instanceof String) {
                                    where.and(field.like("%" + value + "%"));
                                }
                                break;
                            case GREAT:
                                where.and(field.greaterThan(typeValue));
                                break;
                            case LESS:
                                where.and(field.lessThan(typeValue));
                                break;
                            case GREATOREQUAL:
                                where.and(field.greaterOrEqual(typeValue));
                                break;
                            case LESSOREQUAL:
                                where.and(field.lessOrEqual(typeValue));
                                break;
                            case EQUAL:
                                where.and(field.eq(typeValue));
                                break;
                            case IN:
                                where.and(field.in((Object[]) typeValue));
                                break;
                            default:

                        }

                    }

                });
            }
        }

        *//**
         * as => table field
         *
         * @param field
         * @return
         *//*
        private static Field wrap(Field field) {
            if (field instanceof TableField) {
                return field;
            } else {
                try {
                    java.lang.reflect.Field alias = field.getClass().getDeclaredField("alias");
                    alias.setAccessible(true);
                    Object a = alias.get(field);
                    java.lang.reflect.Field wrapped = a.getClass().getDeclaredField("wrapped");
                    wrapped.setAccessible(true);
                    return (Field) wrapped.get(a);
                } catch (NoSuchFieldException | IllegalAccessException e) {
                    e.printStackTrace();
                }
                throw new CommonException("Aliasè½¬ä¹‰åŽŸç”Ÿfieldå¤±è´¥!");
            }
        }


        *//**
         * æŽ’åº�æ�¡ä»¶apply
         *
         * @param where
         * @param sortRules
         *//*
        public static SelectSeekStepN<?> applySort(SelectConditionStep<?> where, String sortRules) throws IOException {
            SelectSeekStepN<?> sortSql = null;
            if (!StringUtils.isBlank(sortRules)) {
                List<SortItemVo> sortItemVos = tranSortRulesJson(sortRules);
                List<OrderField<?>> sortFields = new ArrayList<>();
                if (sortItemVos != null) {
                    Map<String, WhereFieldVo> whereFieldMap = getWhereFiled(where);
                    sortItemVos.forEach((SortItemVo x) -> {
                        if (whereFieldMap.containsKey(x.getField())) {

                            String order = x.getOrder();
                            if (StringUtils.isBlank(order)) {
                                return;
                            }
                            WhereFieldVo vo = whereFieldMap.get(x.getField());
                            Field field = vo.getField();
                            switch (order) {
                                case Order.DESC:
                                    sortFields.add(field.desc());
                                    break;
                                case Order.ASC:
                                    sortFields.add(field.asc());
                                    break;
                                default:
                            }
                        }
                    });
                }
                if (!sortFields.isEmpty()) {
                    sortSql = where.orderBy(sortFields);
                }
            }
            return sortSql;
        }

        *//**
         * ç»Ÿè®¡ä¸ªæ•°
         *
         * @param where
         * @return
         *//*
        public static Integer count(SelectConditionStep<?> where) {
            Configuration configuration = where.configuration();
            DSLContext dsl = new DefaultDSLContext(configuration);
            return dsl.select(DSL.count()).from(where).fetchOne().value1();
        }

        *//**
         * è½¬æ�¢æ•°æ�®ç±»åž‹
         *
         * @param value
         * @param type
         * @param operator
         * @return
         *//*
        private static Object tranObjectType(String value, Class type, String operator) throws ParseException {

            if (!operator.equals(IN)) {
                return tranObjectType(value, type);
            } else {
                String[] arr = value.split(",");
                Object[] oArr = new Object[arr.length];
                for (int i = 0; i < arr.length; i++) {
                    oArr[i] = tranObjectType(arr[i], type);
                }
                return oArr;
            }
        }

        *//**
         * è½¬æ�¢æ•°æ�®ç±»åž‹
         *
         * @param value
         * @param type
         * @return
         *//*
        public static Object tranObjectType(String value, Class type) {
            try {
                if (type == String.class) {
                    return value;
                } else if (type == Integer.class) {
                    return Integer.parseInt(value);
                } else if (type == Double.class) {
                    return Double.parseDouble(value);
                } else if (type == Timestamp.class) {
                    Date date = DateUtils.parseTime(value);
                    return new Timestamp(date.getTime());
                } else if (type == java.sql.Date.class) {
                    return new java.sql.Date(DateUtils.parse(value).getTime());
                } else if (type == BigDecimal.class) {
                    return new BigDecimal(value);
                }
            } catch (Exception e) {
                e.printStackTrace();
                throw new CommonException(type + "è½¬ä¹‰å¤±è´¥:" + value);
            }
            return null;
        }


        public static List<FilterItemVo> tranFilterRulesJson(String json) throws IOException {
            return JsonUtils.decode(json, new com.fasterxml.jackson.core.type.TypeReference<List<FilterItemVo>>() {
            });
        }


        public static List<SortItemVo> tranSortRulesJson(String json) throws IOException {
            return JsonUtils.decode(json, new com.fasterxml.jackson.core.type.TypeReference<List<SortItemVo>>() {
            });
        }

        *//**
         * æŠŠæ•°æ�®åº“å­—æ®µè½¬æ�¢ä¸ºPOJOå­—æ®µ(JOOQæº�ç �)
         *
         * @param dbField
         * @return
         *//*
        public static String tranFieldDBToPojo(String dbField) {
            return toLowerCaseFirstOne(
                    StringUtils.toCamelCase(
                            dbField.replace(' ', '_')
                                    .replace('-', '_')
                                    .replace('.', '_'))
            );
        }


        *//**
         * æŠŠpojoå­—æ®µè½¬æ�¢ä¸ºæ•°æ�®åº“å­—æ®µ
         * æ­¤æ–¹æ³•ä»…å¯¹æ ‡å‡†æ•°æ�®åº“å­—æ®µå‘½å��è§„åˆ™æœ‰æ•ˆ ç±»ä¼¼ user_id å¯¹userIdæ— æ•ˆ
         *
         * @param pojoField
         * @return
         *//*
        public static String tranFieldPojoToDB(String pojoField) {
            StringBuilder sb = new StringBuilder();
            char[] chars = pojoField.toCharArray();
            for (char c : chars) {
                String s = String.valueOf(c);
                //å¦‚æžœæ˜¯å¤§å†™ ä¸”å¿…é¡»æ˜¯å­—æ¯�
                if (s.toUpperCase().equals(s) && Character.isLetter(c)) {
                    //è½¬ä¸º_å°�å†™
                    sb.append("_").append(s.toLowerCase());
                } else {
                    sb.append(s);
                }
            }
            return sb.toString();
        }


        *//**
         * é¦–å­—æ¯�è½¬å°�å†™
         *
         * @param s
         * @return
         *//*
        public static String toLowerCaseFirstOne(String s) {
            if (Character.isLowerCase(s.charAt(0))) {
                return s;
            } else {
                return (new StringBuilder()).append(Character.toLowerCase(s.charAt(0))).append(s.substring(1)).toString();
            }

        }


        *//**
         * èŽ·å�–where field
         *
         * @param where
         * @return
         *//*
        private static Map<String, WhereFieldVo> getWhereFiled(SelectConditionStep<?> where) {
            Map<String, WhereFieldVo> map = new HashMap<>();
            List<Field<?>> fields = where.getSelect();
            fields.forEach(x -> {
                WhereFieldVo vo = new WhereFieldVo();
                vo.setField(x);
                vo.setFieldName(x.getUnqualifiedName().last());
                vo.setPojoFieldName(tranFieldDBToPojo(x.getUnqualifiedName().last()));
                map.put(vo.getPojoFieldName(), vo);
            });
            return map;
        }
    }
*/


