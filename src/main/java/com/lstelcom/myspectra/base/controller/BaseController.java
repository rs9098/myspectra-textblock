package com.lstelcom.myspectra.base.controller;

import com.lstelcom.myspectra.base.common.BaseConstant;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * @author wy130132295
 */
public class BaseController {

    public static final String REQ_MAPPING_URL_API = "/api";


    public BaseController() {

    }

    private static List<DecimalFormat> decimalFormatList = new ArrayList<>();

    static {
        int start = 0;
        int end = 10;
        StringBuilder sb = new StringBuilder();
        for (int i = start; i < end; i++) {
            sb.setLength(0);
            for (int j = 0; j < i; j++) {
                sb.append(BaseConstant.NumberStr.ZERO);
            }
            decimalFormatList.add(new DecimalFormat("#." + sb.toString()));
        }
    }

    /**
     * @param decimal decimal
     * @param num     Keep several decimal places
     * @return decimal string
     */
    public static String decimalFormatToStr(double decimal, int num) {
        return decimalFormatList.get(num).format(decimal);
    }


    /**
     * @param decimal decimal
     * @param num     Keep several decimal places
     * @return decimal
     */
    public static double decimalFormat(double decimal, int num) {
        return Double.parseDouble(decimalFormatList.get(num).format(decimal));
    }

}
