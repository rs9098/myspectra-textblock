package com.lstelcom.myspectra.base.execption;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Setter;

/**
 * @author:wy
 * @date:2020-7-29
 */
@EqualsAndHashCode(callSuper = true)
@Setter
@Data
@AllArgsConstructor
public class CommonException extends RuntimeException {

    private String message;

}
