package com.lstelcom.myspectra.base.execption;

import com.lstelcom.myspectra.base.common.Result;
import io.micronaut.context.annotation.Requires;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.server.exceptions.ExceptionHandler;
import io.micronaut.http.annotation.Produces;

import javax.inject.Singleton;

/**
 * @author:wy
 * @date:2020-7-29
 */
@Singleton
public class ExceptionHandlerStore {

    @Produces
    @Singleton
    @Requires(classes = {CommonException.class, ExceptionHandler.class})
    public static class CommonExceptionHandler implements ExceptionHandler<CommonException, Result> {

        @Override
        public Result handle(HttpRequest request, CommonException exception) {
            return new Result().setMessage(exception.getMessage());
        }
    }
}
