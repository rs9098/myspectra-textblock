/*
 * This file is generated by jOOQ.
 */
package com.lstelcom.myspectra.base.dao.tables.records;


import com.lstelcom.myspectra.base.dao.tables.TextBlock;
import com.lstelcom.myspectra.base.dao.tables.interfaces.ITextBlock;

import java.time.LocalDate;

import javax.annotation.Generated;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record10;
import org.jooq.Row10;
import org.jooq.impl.UpdatableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "https://www.jooq.org",
        "jOOQ version:3.13.1"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
@Entity
@Table(name = "text_block", uniqueConstraints = {
    @UniqueConstraint(name = "text_block_pk", columnNames = {"id"})
})
public class TextBlockRecord extends UpdatableRecordImpl<TextBlockRecord> implements Record10<Integer, String, String, String, String, String, String, String, LocalDate, String>, ITextBlock {

    private static final long serialVersionUID = -426328960;

    /**
     * Setter for <code>text_block.id</code>. id
     */
    @Override
    public void setId(Integer value) {
        set(0, value);
    }

    /**
     * Getter for <code>text_block.id</code>. id
     */
    @Id
    @Column(name = "id", nullable = false, precision = 32)
    @NotNull
    @Override
    public Integer getId() {
        return (Integer) get(0);
    }

    /**
     * Setter for <code>text_block.service</code>.
     */
    @Override
    public void setService(String value) {
        set(1, value);
    }

    /**
     * Getter for <code>text_block.service</code>.
     */
    @Column(name = "service", nullable = false, length = 50)
    @NotNull
    @Size(max = 50)
    @Override
    public String getService() {
        return (String) get(1);
    }

    /**
     * Setter for <code>text_block.subService</code>.
     */
    @Override
    public void setSubservice(String value) {
        set(2, value);
    }

    /**
     * Getter for <code>text_block.subService</code>.
     */
    @Column(name = "subService", nullable = false, length = 50)
    @NotNull
    @Size(max = 50)
    @Override
    public String getSubservice() {
        return (String) get(2);
    }

    /**
     * Setter for <code>text_block.category1</code>.
     */
    @Override
    public void setCategory1(String value) {
        set(3, value);
    }

    /**
     * Getter for <code>text_block.category1</code>.
     */
    @Column(name = "category1", nullable = false, length = 50)
    @NotNull
    @Size(max = 50)
    @Override
    public String getCategory1() {
        return (String) get(3);
    }

    /**
     * Setter for <code>text_block.category2</code>.
     */
    @Override
    public void setCategory2(String value) {
        set(4, value);
    }

    /**
     * Getter for <code>text_block.category2</code>.
     */
    @Column(name = "category2", length = 50)
    @Size(max = 50)
    @Override
    public String getCategory2() {
        return (String) get(4);
    }

    /**
     * Setter for <code>text_block.category3</code>.
     */
    @Override
    public void setCategory3(String value) {
        set(5, value);
    }

    /**
     * Getter for <code>text_block.category3</code>.
     */
    @Column(name = "category3", length = 50)
    @Size(max = 50)
    @Override
    public String getCategory3() {
        return (String) get(5);
    }

    /**
     * Setter for <code>text_block.comment</code>.
     */
    @Override
    public void setComment(String value) {
        set(6, value);
    }

    /**
     * Getter for <code>text_block.comment</code>.
     */
    @Column(name = "comment", length = 512)
    @Size(max = 512)
    @Override
    public String getComment() {
        return (String) get(6);
    }

    /**
     * Setter for <code>text_block.user</code>.
     */
    @Override
    public void setUser(String value) {
        set(7, value);
    }

    /**
     * Getter for <code>text_block.user</code>.
     */
    @Column(name = "user", nullable = false, length = 100)
    @NotNull
    @Size(max = 100)
    @Override
    public String getUser() {
        return (String) get(7);
    }

    /**
     * Setter for <code>text_block.dateOfRequest</code>.
     */
    @Override
    public void setDateofrequest(LocalDate value) {
        set(8, value);
    }

    /**
     * Getter for <code>text_block.dateOfRequest</code>.
     */
    @Column(name = "dateOfRequest", nullable = false)
    @NotNull
    @Override
    public LocalDate getDateofrequest() {
        return (LocalDate) get(8);
    }

    /**
     * Setter for <code>text_block.languageAndTextPairs</code>.
     */
    @Override
    public void setLanguageandtextpairs(String value) {
        set(9, value);
    }

    /**
     * Getter for <code>text_block.languageAndTextPairs</code>.
     */
    @Column(name = "languageAndTextPairs", nullable = false, length = 50)
    @NotNull
    @Size(max = 50)
    @Override
    public String getLanguageandtextpairs() {
        return (String) get(9);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    @Override
    public Record1<Integer> key() {
        return (Record1) super.key();
    }

    // -------------------------------------------------------------------------
    // Record10 type implementation
    // -------------------------------------------------------------------------

    @Override
    public Row10<Integer, String, String, String, String, String, String, String, LocalDate, String> fieldsRow() {
        return (Row10) super.fieldsRow();
    }

    @Override
    public Row10<Integer, String, String, String, String, String, String, String, LocalDate, String> valuesRow() {
        return (Row10) super.valuesRow();
    }

    @Override
    public Field<Integer> field1() {
        return TextBlock.TEXT_BLOCK.ID;
    }

    @Override
    public Field<String> field2() {
        return TextBlock.TEXT_BLOCK.SERVICE;
    }

    @Override
    public Field<String> field3() {
        return TextBlock.TEXT_BLOCK.SUBSERVICE;
    }

    @Override
    public Field<String> field4() {
        return TextBlock.TEXT_BLOCK.CATEGORY1;
    }

    @Override
    public Field<String> field5() {
        return TextBlock.TEXT_BLOCK.CATEGORY2;
    }

    @Override
    public Field<String> field6() {
        return TextBlock.TEXT_BLOCK.CATEGORY3;
    }

    @Override
    public Field<String> field7() {
        return TextBlock.TEXT_BLOCK.COMMENT;
    }

    @Override
    public Field<String> field8() {
        return TextBlock.TEXT_BLOCK.USER;
    }

    @Override
    public Field<LocalDate> field9() {
        return TextBlock.TEXT_BLOCK.DATEOFREQUEST;
    }

    @Override
    public Field<String> field10() {
        return TextBlock.TEXT_BLOCK.LANGUAGEANDTEXTPAIRS;
    }

    @Override
    public Integer component1() {
        return getId();
    }

    @Override
    public String component2() {
        return getService();
    }

    @Override
    public String component3() {
        return getSubservice();
    }

    @Override
    public String component4() {
        return getCategory1();
    }

    @Override
    public String component5() {
        return getCategory2();
    }

    @Override
    public String component6() {
        return getCategory3();
    }

    @Override
    public String component7() {
        return getComment();
    }

    @Override
    public String component8() {
        return getUser();
    }

    @Override
    public LocalDate component9() {
        return getDateofrequest();
    }

    @Override
    public String component10() {
        return getLanguageandtextpairs();
    }

    @Override
    public Integer value1() {
        return getId();
    }

    @Override
    public String value2() {
        return getService();
    }

    @Override
    public String value3() {
        return getSubservice();
    }

    @Override
    public String value4() {
        return getCategory1();
    }

    @Override
    public String value5() {
        return getCategory2();
    }

    @Override
    public String value6() {
        return getCategory3();
    }

    @Override
    public String value7() {
        return getComment();
    }

    @Override
    public String value8() {
        return getUser();
    }

    @Override
    public LocalDate value9() {
        return getDateofrequest();
    }

    @Override
    public String value10() {
        return getLanguageandtextpairs();
    }

    @Override
    public TextBlockRecord value1(Integer value) {
        setId(value);
        return this;
    }

    @Override
    public TextBlockRecord value2(String value) {
        setService(value);
        return this;
    }

    @Override
    public TextBlockRecord value3(String value) {
        setSubservice(value);
        return this;
    }

    @Override
    public TextBlockRecord value4(String value) {
        setCategory1(value);
        return this;
    }

    @Override
    public TextBlockRecord value5(String value) {
        setCategory2(value);
        return this;
    }

    @Override
    public TextBlockRecord value6(String value) {
        setCategory3(value);
        return this;
    }

    @Override
    public TextBlockRecord value7(String value) {
        setComment(value);
        return this;
    }

    @Override
    public TextBlockRecord value8(String value) {
        setUser(value);
        return this;
    }

    @Override
    public TextBlockRecord value9(LocalDate value) {
        setDateofrequest(value);
        return this;
    }

    @Override
    public TextBlockRecord value10(String value) {
        setLanguageandtextpairs(value);
        return this;
    }

    @Override
    public TextBlockRecord values(Integer value1, String value2, String value3, String value4, String value5, String value6, String value7, String value8, LocalDate value9, String value10) {
        value1(value1);
        value2(value2);
        value3(value3);
        value4(value4);
        value5(value5);
        value6(value6);
        value7(value7);
        value8(value8);
        value9(value9);
        value10(value10);
        return this;
    }

    // -------------------------------------------------------------------------
    // FROM and INTO
    // -------------------------------------------------------------------------

    @Override
    public void from(ITextBlock from) {
        setId(from.getId());
        setService(from.getService());
        setSubservice(from.getSubservice());
        setCategory1(from.getCategory1());
        setCategory2(from.getCategory2());
        setCategory3(from.getCategory3());
        setComment(from.getComment());
        setUser(from.getUser());
        setDateofrequest(from.getDateofrequest());
        setLanguageandtextpairs(from.getLanguageandtextpairs());
    }

    @Override
    public <E extends ITextBlock> E into(E into) {
        into.from(this);
        return into;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached TextBlockRecord
     */
    public TextBlockRecord() {
        super(TextBlock.TEXT_BLOCK);
    }

    /**
     * Create a detached, initialised TextBlockRecord
     */
    public TextBlockRecord(Integer id, String service, String subservice, String category1, String category2, String category3, String comment, String user, LocalDate dateofrequest, String languageandtextpairs) {
        super(TextBlock.TEXT_BLOCK);

        set(0, id);
        set(1, service);
        set(2, subservice);
        set(3, category1);
        set(4, category2);
        set(5, category3);
        set(6, comment);
        set(7, user);
        set(8, dateofrequest);
        set(9, languageandtextpairs);
    }
}
