package com.lstelcom.myspectra.base.utils;


import java.io.File;
import java.util.UUID;

/**
 * @author:wy
 * @date:2020-6-12
 */
public class FileUtils {

    public static File createFile(String dir) {
        File dest = new File(dir + File.separator + UUID.randomUUID().toString());
        File parent = dest.getParentFile();
        if (parent != null && !parent.exists()) {
            parent.mkdirs();
        }
        return dest;
    }

}
