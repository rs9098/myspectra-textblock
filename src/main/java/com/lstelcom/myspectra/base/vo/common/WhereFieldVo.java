package com.lstelcom.myspectra.base.vo.common;

import com.lstelcom.myspectra.base.common.BaseVO;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.jooq.Field;

/**
 * @author:wy
 * @date:2020/1/17
 */
@Getter
@Setter
@Accessors(chain = true)
public class WhereFieldVo extends BaseVO {

    private String fieldName;

    private String pojoFieldName;

    private Field<?> field;

}
