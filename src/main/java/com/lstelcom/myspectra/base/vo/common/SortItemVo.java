package com.lstelcom.myspectra.base.vo.common;

import com.lstelcom.myspectra.base.common.BaseVO;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * @author:wy
 * @date:2020/1/17
 */
@Getter
@Setter
@Accessors(chain = true)
public class SortItemVo extends BaseVO {

    private String field;

    private String order;

}
